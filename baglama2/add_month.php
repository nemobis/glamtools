#!/usr/bin/php
<?PHP

# cd /data/project/glamtools/baglama2
# jsub -mem 4g -cwd ./add_month.php YEAR MONTH

# DB is s51203__baglama2_p

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','3000M');
//set_time_limit ( 60 * 60 * 10 ) ;


if ( !isset ( $argv[2] ) ) {
	print "Usage: add_month.php YEAR MONTH\n" ;
	print "Adds all pages for the groups in that month, where not exists\n" ;
	exit ( 1 ) ;
}
$year = $argv[1] * 1 ;
$month = $argv[2] * 1 ;

include_once ( '../public_html/php/common.php' ) ;
$commons = openDB ( 'commons' , 'wikimedia' ) ;
$db = openToolDB ( 'baglama2_p' ) ;

$sites = array() ;

// ____________________________________________________________________________________________________________

// Ensure all globalimagelinks sites exist in site table
function ensureSites () {
	global $commons , $db , $sites ;
	
	$existing = array() ;
	$sql = "SELECT * FROM sites" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$existing[$o->giu_code] = $o ;
	}
	
	$sql = "select distinct gil_wiki from globalimagelinks" ;
	if(!$result = $commons->query($sql)) die('There was an error running the query [' . $commons->error . ']');
	while($o = $result->fetch_object()){
		if ( isset ( $existing[$o->gil_wiki] ) ) continue ;
		
		
		preg_match ( '/^(.+)(wik.+)$/' , $o->gil_wiki , $parts ) ;
		if ( count ( $parts ) != 3 ) {
			print "Unusual wiki, aborting :\n" ;
			print_r ( $parts ) ;
			exit ( 0 ) ;
		}
		
		$giu_code = $o->gil_wiki ;
		$language = str_replace ( '_' , '-' , $parts[1] ) ;
		$project = $parts[2] ;
		if ( $language == 'commons' ) $project = 'wikimedia' ;
		if ( substr ( $language , 0 , 9 ) == 'wikimania' ) $project = 'wikimedia' ;
		if ( $project == 'wiki' ) $project = 'wikipedia' ;
		$server = "$language.$project.org" ;
		
		$sql = "INSERT INTO sites (grok_code,server,giu_code,project,language) VALUES ('','$server','$giu_code','$project','$language')" ;
		if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	}

	// As above, so below
	$sql = "SELECT * FROM sites" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$sites[$o->giu_code] = $o ;
	}
}

function addPages ( $g ) {
	global $commons , $db , $sites , $month , $year ;
	
	$g->category = utf8_encode($g->category) ;
	
	print $g->category . "\n" ;
	
	$cats = array() ;
	findSubcats ( $commons , array($g->category) , $cats , $g->depth ) ;


	$sql = "INSERT INTO group_status (group_id,year,month,status) VALUES (".$g->id.",$year,$month,'GENERATING PAGE LIST') on duplicate key update status=values(status)" ;
	if(!$r2 = $db->query($sql)) die('There was an error running the query 3 [' . $db->error . ']');
	$group_status_id = $db->insert_id ;

	$sql = "SELECT globalimagelinks.* FROM globalimagelinks,page,categorylinks WHERE gil_to=page_title AND cl_from=page_id AND page_namespace=6 AND cl_to IN ('" . implode("','",$cats) . "') AND cl_timestamp < '" . $year . "-" . $month . "-31'" ; //  AND page_is_redirect=0

	$db->autocommit(false) ;
	$cnt = 0 ;
	
	if(!$result = $commons->query($sql)) die('There was an error running the query 1 [' . $commons->error . ']');
	while($o = $result->fetch_object()){
		if ( !isset($sites[$o->gil_wiki]) ) {
			print "Unknown site " . $o->gil_wiki . ", skipping.\n" ;
			continue ;
		}
		
		$title = $o->gil_page_title ;
		
		$sql = "INSERT IGNORE INTO views (site,page_id,title,month,year,done,namespace_id) VALUES (" ;
		$sql .= $sites[$o->gil_wiki]->id . "," ;
		$sql .= $o->gil_page . "," ;
		$sql .= "'" . $db->real_escape_string($title) . "'," ;
		$sql .= "$month,$year,0," ;
		$sql .= $o->gil_page_namespace_id . ")" ;

		if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$last_id = $db->insert_id ;
		
		if ( $last_id == 0 ) {
			$sql = "INSERT IGNORE INTO group2view (group_status_id,view_id,image) SELECT " ;
			$sql .= $group_status_id . ",views.id,'" . $db->real_escape_string($o->gil_to) ;
			$sql .= "' FROM views WHERE" ;
			$sql .= " views.site=" . $sites[$o->gil_wiki]->id . " AND " ;
			$sql .= " views.page_id=" . $o->gil_page . " AND " ;
			$sql .= " views.month=$month AND views.year=$year LIMIT 1" ;
		} else {
			$sql = "INSERT IGNORE INTO group2view (group_status_id,view_id,image) VALUES (" ;
			$sql .= $group_status_id . "," ;
			$sql .= $last_id . "," ;
			$sql .= "'" . $db->real_escape_string($o->gil_to) . "')" ;
		}
		if(!$r2 = $db->query($sql)) die('There was an error running the query 2 [' . $db->error . ']');
	
	
		$cnt++ ;
		if ( $cnt % 10000 == 0 ) $db->commit() ;
	}

	$db->autocommit(true) ;
	
	$sql = "INSERT INTO group_status (group_id,year,month,status) VALUES (".$g->id.",$year,$month,'PAGE LIST GENERATED') on duplicate key update status=values(status)" ;
	if(!$r2 = $db->query($sql)) die('There was an error running the query 3 [' . $db->error . ']');
}


// ____________________________________________________________________________________________________________


ensureSites() ;

$sql = "SELECT * FROM groups WHERE NOT EXISTS (SELECT * FROM group_status WHERE group_status.group_id=groups.id AND group_status.month=$month AND group_status.year=$year LIMIT 1)" ;
if(!$result = $db->query($sql)) die('There was an error running the query 0 [' . $db->error . ']');
while($o = $result->fetch_object()){
	addPages ( $o ) ;
}

print "COMPLETION\n" ;

?>