#!/usr/bin/php
<?PHP

# cd /data/project/glamtools/baglama2
# jsub -mem 4g -cwd ./load_viewdata.php YEAR MONTH

# DB is s51203__baglama2_p

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','3000M');
//set_time_limit ( 60 * 60 * 10 ) ;


if ( !isset ( $argv[2] ) ) {
	print "Usage: add_month.php YEAR MONTH\n" ;
	print "Adds all pages for the groups in that month, where not exists\n" ;
	exit ( 1 ) ;
}
$year = $argv[1] * 1 ;
$month = $argv[2] * 1 ;
$ym = $year . ( $month < 10 ? "0".$month : $month ) ;

include_once ( '../public_html/php/common.php' ) ;
$db = openToolDB ( 'baglama2_p' ) ;

function getViewCounts ( $gs ) {
	global $db , $ym ;
	
	$batch = 2000 ;
	
	$found = true ;
	while ( $found ) {
		$found = false ;
		$sql = "SELECT DISTINCT views.id AS id,title,namespace_id,grok_code FROM views,group2view,sites WHERE group_status_id=" . $gs->id . " AND view_id=views.id AND sites.id=site AND done=0 AND grok_code!='' AND namespace_id=0 ORDER BY rand() LIMIT $batch" ;
		if(!$result = $db->query($sql)) die('There was an error running the query 1: $sql [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$url = "http://stats.grok.se/json/" . $o->grok_code . "/$ym/" . $o->title ;
			$data = file_get_contents ( $url ) ;
			if ( $data === false || $data == '' ) {
				print "No view data for $url\n" ;
				continue ;
			}
			$data = json_decode ( $data ) ;
			$total = 0 ;
			foreach ( $data->daily_views AS $v ) $total += $v*1 ;
			$sql2 = "UPDATE views SET done=1,views=$total WHERE id=" . $o->id ;
			if(!$result2 = $db->query($sql2)) die('There was an error running the query 2: $sql2 [' . $db->error . ']');
			$found = true ;
		}
	}
	
	$total_views = 0 ;
	$sql = "SELECT SUM(views) AS total_views FROM views,sites WHERE views.id IN (SELECT DISTINCT view_id FROM group2view WHERE group_status_id=".$gs->id.") AND sites.id=site AND done=1 AND grok_code!='' AND namespace_id=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query 3: $sql [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$total_views = $o->total_views ;
	}
	
	if ( $total_views == null or $total_views === false ) $total_views = 0 ;
	
	$sql = "UPDATE group_status SET status='VIEW DATA COMPLETE',total_views=$total_views WHERE id=" . $gs->id ;
	if(!$result = $db->query($sql)) die("There was an error running the query 4: $sql [" . $db->error . ']');
	
	$sql = "INSERT IGNORE INTO gs2site (group_status_id,site_id,pages,views) SELECT ".$gs->id.",site,count(*),sum(views.views) AS cnt FROM views WHERE done=1 AND views.id IN (SELECT DISTINCT view_id FROM group2view WHERE group_status_id=".$gs->id.") GROUP BY site" ;
	if(!$result = $db->query($sql)) die("There was an error running the query 5: $sql [" . $db->error . ']');
	
}


$sql = "SELECT * FROM group_status WHERE year=$year AND month=$month AND status='PAGE LIST GENERATED' order by rand()" ;
//$sql .= " AND id=9" ; // TESTING
if(!$result = $db->query($sql)) die('There was an error running the query 0 [' . $db->error . ']');
while($o = $result->fetch_object()){
	getViewCounts ( $o ) ;
//	break ; // TESTING
}

print "COMPLETION\n" ;


?>